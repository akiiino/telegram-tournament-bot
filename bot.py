import json
import pprint
import pickle
import random
from flask import Flask, request, g
import requests
import numpy as np

app = Flask(__name__)

def get_data():
    if 'scores' not in g or 'last_roll' not in g:
        try:
            g.scores, g.last_roll = pickle.load(open("data/dump.pkl", "rb"))
        except (FileNotFoundError, pickle.UnpicklingError):
            g.scores = {}
            g.last_roll = {}

    return g.scores, g.last_roll


@app.teardown_request
def save_updates(_exception=None):
    if 'scores' not in g:
        get_data()
    with open("data/dump.pkl", "wb") as dumpfile:
        pickle.dump((g.scores, g.last_roll), dumpfile)


def send_request(method, files=None, **kwargs):
    """
    Execute a method from the Telegram Bot API

    Args:
        method (str): the method's name
        token_param (str): bot's token
        files (dict): binary files to attach to request
        **kwargs: the method's parameters
    """
    if "token" not in g:
        g.token = open("data/token.dat").read().strip()
        assert send_request("getMe")["ok"]
        assert send_request("setWebhook", url=f"https://akiiino.me:8443",
                            allowed_updates=["message"])["ok"]

    return requests.post(
        f"https://api.telegram.org/bot{g.token}/{method}",
        params=kwargs,
        files=files
    ).json()

SIZES = {"🎲": 6, "🎯": 6, "🏀": 5, "⚽": 5, "🎰": 64}
RESET_TIME = 60 * 60 * 24

def get_init(size):
    return dict(enumerate([0 for _ in range(size)], 1))

def make_roll(message):
    scores, last_roll = get_data()

    chat_id = message["chat"]["id"]
    roller = message["from"]["id"]
    dice = message["dice"]["emoji"]

    if chat_id not in last_roll:
        last_roll[chat_id] = {}
    if chat_id not in scores:
        scores[chat_id] = {}

    if roller not in last_roll[chat_id]:
        last_roll[chat_id][roller] = {die: 0 for die in SIZES}
    if roller not in scores[chat_id]:
        scores[chat_id][roller] = {die: get_init(size) for die, size in SIZES.items()}

    if dice not in last_roll[chat_id][roller] or last_roll[chat_id][roller][dice] // RESET_TIME < message["date"] // RESET_TIME:
        last_roll[chat_id][roller][dice] = message["date"]

        scores[chat_id][roller][dice][message["dice"]["value"]] += 1
    else:
        del_req = send_request("deleteMessage", chat_id=chat_id, message_id=message["message_id"])
        if not del_req["ok"]:
            print("Couldn't delete message! See below:")
            pprint.pprint(del_req)
            pprint.pprint(message)
        if random.randint(1, 100) == 100:
            name = send_request(
                "getChatMember",
                chat_id=chat_id,
                user_id=roller
            )["result"]["user"]["username"]
            send_request(
                "sendAnimation",
                chat_id=chat_id,
                animation="CgACAgIAAxkDAAIBDF7zl5SvXOGjgJRuzBq29_hBe9ycAAJ5CQACezahSx7WypLvBjp4GgQ",
                caption=f"[@{name}](tg://user?id={roller})",
                parse_mode="MarkdownV2")


def get_top(chat_id, die, mode, get_best=True):
    scores, _ = get_data()
    die_scores = {roller: sum(k*v for k, v in table[die].items()) for roller, table in scores[chat_id].items()}
    die_scores = {roller: score for roller, score in die_scores.items() if score != 0}
    if mode == "avg":
        die_scores = {roller: score/sum(scores[chat_id][roller][die].values()) for roller, score in die_scores.items() if sum(scores[chat_id][roller][die].values()) >= 3}

    return sorted(die_scores.items(), key=lambda x: x[1], reverse=get_best)


def escape(string):
    return string.translate(str.maketrans({
        "_":  r"\_", "*":  r"\*", "[":  r"\[", "]":  r"\]", "(":  r"\(", ")":  r"\)",
        "~":  r"\~", "`":  r"\`", ">":  r"\>", "#":  r"\#", "+":  r"\+", "-":  r"\-",
        "=":  r"\=", "|":  r"\|", "{":  r"\{", "}":  r"\}", ".":  r"\.", "!":  r"\!",
    }))

def display_leaders(message):
    chat_id = message["chat"]["id"]
    command = message["text"].split()[0]
    best = "anti" not in command
    mode = "avg" if "avg" in command else "sum"
    response = "*"
    response += "Highest" if best else "Lowest"
    response += " "
    response += "average" if mode == "avg" else "scoring"
    response += " rollers:*\n"
    for die, size in SIZES.items():
        response += f"\n{die}\n"
        leaders = get_top(chat_id, die, mode, best)
        for roller, score in leaders[:3]:
            name = send_request("getChatMember", chat_id=chat_id, user_id=roller)
            if not name["ok"]:
                name = str(roller)
                link = f"tg://user?id={name}"
            else:
                name = name["result"]["user"]["username"]
                link = f"https://t.me/{name}"
            if mode == "avg":
                score = f"{score:.3g}"
                response += f"[{escape(name)}]({link}): {escape(score)}"+r"\/"+f"{size}\n"
            else:
                score = f"{score:.0f}"
                response += f"[{escape(name)}]({link}): {escape(score)}\n"
    send_request("sendMessage", chat_id=chat_id, text=response, reply_to_message_id=message["message_id"], disable_notification=True, parse_mode="MarkdownV2", disable_web_page_preview=True)


def get_percentile(sides, rolls, total):
    def add_roll(probs, base_probs):
        res = np.zeros(len(probs[1]) + len(base_probs[1])-1)
        for roll_x, prob_x in enumerate(probs[1]):
            for roll_y, prob_y in enumerate(base_probs[1]):
                res[roll_x+roll_y] += prob_x*prob_y
        return (probs[0] + base_probs[0], res)

    if not rolls <= total <= sides*rolls:
        return 0
    probs = (1, np.ones(sides)/sides)

    curr_total = 1
    precomp = {1: probs}
    while curr_total != rolls:
        for add, probs_b in precomp.items():
            if curr_total + add <= rolls:
                probs = add_roll(probs, probs_b)
                curr_total += add
                precomp[curr_total] = probs
                break

    return np.cumsum(probs[1][::-1])[::-1][total-probs[0]] * 100


def get_stats(message):
    scores, _ = get_data()
    chat_id = message["chat"]["id"]
    roller = message["from"]["id"]

    if "reply_to_message" in message:
        message = message["reply_to_message"]
    roller = message["from"]["id"]

    name = send_request("getChatMember", chat_id=chat_id, user_id=roller)["result"]["user"]["username"]

    try:
        rolls = scores[chat_id][roller].items()
        roller_sum_scores = {die: sum(k*v for k, v in score.items()) for die, score in rolls}
        roller_num_scores = {die: sum(score.values()) for die, score in rolls}
        roller_avg_scores = {die: sum(k*v for k, v in score.items())/sum(score.values()) for die, score in rolls}
        roller_scores = {"sum": roller_sum_scores, "avg": roller_avg_scores, "num": roller_num_scores}
    except (ZeroDivisionError, KeyError):
        response = f"*[{escape(name)}](https://t.me/{name})* is yet to roll every die at least once\n"
        send_request("sendMessage", chat_id=chat_id, text=response, reply_to_message_id=message["message_id"], disable_notification=True, parse_mode="MarkdownV2", disable_web_page_preview=True)
        return

    response = f"*[{escape(name)}](https://t.me/{name})'s stats:*\n"
    for mode in ["sum", "avg"]:
        if mode == "sum":
            response += "\nTotal:\n"
        elif mode == "avg":
            response += "\nAverage:\n"
        for die, size in SIZES.items():
            response += f"{die}"
            score = roller_scores[mode][die]
            rolls = roller_scores["num"][die]
            if mode == "avg":
                sum_score = roller_scores["sum"][die]
                perc = get_percentile(size, rolls, sum_score)
                score = f"{score:.3g}"
                if perc <= 50:
                    perc = escape(f"{perc:.3g}")
                    response += f": {escape(score)}" + r"\/" + f"{size}; top {perc}%\n"
                else:
                    perc = escape(f"{100-perc:.3g}")
                    response += f": {escape(score)}" + r"\/" + f"{size}; bottom {perc}%\n"
            elif mode == "sum":
                score = f"{score:.0f}"
                response += f": {escape(score)}; {rolls} roll" + ("s" if rolls > 1 else "") + "\n"
    send_request("sendMessage", chat_id=chat_id, text=response, reply_to_message_id=message["message_id"], disable_notification=True, parse_mode="MarkdownV2", disable_web_page_preview=True)

def update_dice():
    scores, last_roll = get_data()

    for die in SIZES:
        for chat_id in scores:
            for roller in scores[chat_id]:
                if die not in scores[chat_id][roller]:
                    scores[chat_id][roller][die] = get_init(SIZES[die])

    for die in SIZES:
        for chat_id in last_roll:
            for roller in last_roll[chat_id]:
                if die not in last_roll[chat_id][roller]:
                    last_roll[chat_id][roller][die] = 0
    with open("data/dump.pkl", "wb") as dumpfile:
        pickle.dump((g.scores, g.last_roll), dumpfile)
    

@app.route(f'/', methods=['POST'])
def main_loop():
    if "is_initialized" not in g:
        update_dice()
        g.is_initialized = True

    data = json.loads(request.data)
    if "message" not in data:
        return "OK"

    if "dice" in data["message"] and "forward_date" not in data["message"]:
        make_roll(data["message"])

    if "text" in data["message"] and data["message"]["text"].startswith("/get_top"):
        display_leaders(data["message"])

    if "text" in data["message"] and data["message"]["text"].startswith("/get_antitop"):
        display_leaders(data["message"])

    if "text" in data["message"] and data["message"]["text"].startswith("/get_stats"):
        get_stats(data["message"])

    return "OK"
