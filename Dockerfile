FROM python:3

RUN groupadd -g 61000 docker_user && useradd -g 61000 -l -m -s /bin/false -u 61000 docker_user
USER docker_user
ENV PATH="/home/docker_user/.local/bin:${PATH}"

WORKDIR /usr/src/bot

COPY requirements.txt ./
RUN pip install --user --no-cache-dir -r requirements.txt

COPY bot.py ./

ENV PYTHONUNBUFFERED TRUE
ENTRYPOINT gunicorn -t 300 --worker-tmp-dir /dev/shm --certfile /usr/src/certs/live/$domain/fullchain.pem --keyfile /usr/src/certs/live/$domain/privkey.pem -b 0.0.0.0:8443 bot:app
